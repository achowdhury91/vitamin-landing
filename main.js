$(document).ready(function() {

	var controller = new ScrollMagic.Controller();

	// pin the home screen
	var pinIntroScene = new ScrollMagic.Scene({
		triggerElement: '#intro',
		triggerHook: 0
	})
	.setPin('#intro')
	.addTo(controller);

	// build a scene (defines animations)
	var ourScene = new ScrollMagic.Scene({
		// options for the scene
		triggerElement: '#screen01',
		duration: '100%',
		triggerHook: 0.4,
		// reverse: false
	})
	.setClassToggle('#screen01 .content', 'fade-in') // add class to project01
	.addIndicators({
		name: 'fade scene',
		colorTrigger: 'black',
		colorStart: '#75c695',
		colorEnd: 'pink'
	}) // this requires the addIndicators plugin
	.addTo(controller);

	// pin the video screen
	var pinVideoScene = new ScrollMagic.Scene({
		triggerElement: '#screen01',
		triggerHook: 0
	})
	.setPin('#screen01')
	.addTo(controller);

	// pink block animation in screen03
	var pinkBlockTimeline = new TimelineMax();
	pinkBlockTimeline
		.fromTo(
			'.pinkBlock',
			2,
			{
				y: '+=50%'
			},
			{
				y: '-=50%'
			}
		);

	var pinkBlockScene = new ScrollMagic.Scene({
		triggerElement: '#screen02',
		triggerHook: 0.4
	})
	.setTween(pinkBlockTimeline)
	.addIndicators({
		name: 'pinkBlock',
		colorTrigger: 'red',
		colorStart: 'orange'
	})
	.addTo(controller);

	// pin the pinkBlock screen
	var pinVideoScene = new ScrollMagic.Scene({
		triggerElement: '#screen02',
		triggerHook: 0
	})
	.setPin('#screen02')
	.addTo(controller);

});
